
import time
import RPi.GPIO as GPIO

# RPi.GPIO Layout verwenden (wie Pin-Nummern)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

# Pin 7 (GPIO 4) auf Output setzen
GPIO.setup(7, GPIO.OUT)

# Dauersschleife
while 1:
	# LED immer ausmachen
	GPIO.output(7, GPIO.LOW)
  	time.sleep(2.0)
	GPIO.output(7, GPIO.HIGH)
  	time.sleep(2.0)
