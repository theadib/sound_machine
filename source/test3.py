#!/usr/bin/env python

import socket
import select 
import os

UDP_PORT = 8888

print("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('', UDP_PORT))


print('hello')

filenames = dict()
for filename in [f for f in os.listdir()]:
    if os.path.isfile(filename) \
            and filename.lower().endswith('.wav') \
            or filename.lower().endswith('.mp3'):
        [filebase, fileext] = os.path.splitext(filename.lower())
        print(filename, filebase)
        filenames[filebase] = filename

print(filenames)

while True:
    ready = select.select([sock], [], [], 1)
    if ready[0]:
        # data = sock.recv(4096)
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        print("received message:", data)
        start = data.find(b'\2')
        end = data.find(b'\3')
        ctrl = data.find(b'\x11')
        print(start, ctrl, end)
        basename = ''
        if ctrl == start+1 and ctrl < end:
            basename = data[ctrl+1:end].decode("utf-8").lower()
        elif start >= 0 and end > start:
            basename = data[start+1:end].decode("utf-8").lower()
        print(basename)
        if basename in filenames:
            print(filenames[basename])
    print('.')
    
