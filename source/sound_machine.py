#!/usr/bin/env python3
'''
     a note on fileformats:
     according https://stackoverflow.com/questions/49037853/pygame-sound-unable-to-open-file

     Sound can be loaded from an OGG audio file or from an uncompressed WAV.

'''

import socket
import select
import time
import os
import time
import pygame
import asyncio
import json
import sys

try:
    import RPi.GPIO as GPIO
    isRaspi = True
except ModuleNotFoundError:
    isRaspi = False

version = "2.0"
RPC_PORT = 6002
UDP_PORT = 8888
'''
config is is read from sound_machine.json on startup
{
    "sounds":{
        "K00":["", 0, 100],
        "K99":["hupe_2sec.wav", 0, 100],
        "K20":["hupe_2sec.wav", 0.2, 20],
        "K50":["hupe_2sec.wav", 0.5, 100],
        "K80":["hupe_2sec.wav", 0.8, 100]
    },
    "udp_port":8888
}
'''
config = {}
sound_isactive = False
current_sound = None

def led_init():
    if not isRaspi:
        return
    # RPi.GPIO Layout verwenden (wie Pin-Nummern)
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)

    # Pin 7 (GPIO 4) auf Output setzen
    GPIO.setup(7, GPIO.OUT)

def led_on():
    if not isRaspi:
        # print('on')
        return
    GPIO.output(7, GPIO.HIGH)
    
def led_off():
    if not isRaspi:
        # print('off')
        return
    GPIO.output(7, GPIO.LOW)
    
async def echo(message):
    print('echo')
    return message

async def getVersion(param):
    global version
    return version

# sound command Asynchronous 
async def asound(filename, delay=0, volume=100):
    ''' param: soundfilename[, maxlength] '''
    global current_sound
    print('asound(): ', filename, delay, volume)
    if current_sound:
        current_sound.stop()
    try:
        current_sound = pygame.mixer.Sound(filename)
        current_sound.set_volume(volume / 100.0)
        current_sound.play()
    except FileNotFoundError:
        current_sound = None
    delay = float(delay)
    if current_sound and delay>0:
        if delay>10:
            delay=10
        await asyncio.sleep(delay)
        current_sound.stop()
        current_sound = None
    return 0

async def getVersion(param):
    global version
    return version


# sound command Synchronous 
def sound(filename, delay=0, volume=100):
    ''' param: soundfilename[, maxlength] '''
    global current_sound
    print('sound():', filename, delay, volume)
    if current_sound:
        current_sound.stop()
    try:
        current_sound = pygame.mixer.Sound(filename)
        current_sound.set_volume(volume / 100.0)
        current_sound.play()
    except FileNotFoundError:
        current_sound = None
    delay = float(delay)
    if current_sound and delay>0:
        if delay>10:
            delay=10
        time.sleep(delay)
        current_sound.stop()
        current_sound = None
    return 0


async def getConfig(param):
    global config
    return config

async def setConfig(param):
    global config
    config = param.copy()
    config["version"] = version
    config["rpc_port"] = RPC_PORT
    # write back to disc
    with open('lson002.json', 'w') as outfile:
        json.dump(config, outfile, indent=4)

    return config
    

   
dispatcher = { 'echo':echo, 'version':getVersion, 'sound':sound, 'getconfig':getConfig, 'setconfig':setConfig }

async def handle_jsonrpc(reader, writer):
    data = b''
    while True:
        data += await reader.read(100)
        print(data)
        if data.count(b'{') == data.count(b'}'):
            break
    
    message = data.decode()
    addr = writer.get_extra_info('peername')
    print("Received %r from %r" % (message, addr))

    request = json.loads(message)
    print(request)
    response = { 'jsonrpc': '2.0', 'id': request['id'] }
    methodname = request['method'].lower()
    if methodname in dispatcher:
        try:
            f = dispatcher[methodname]
            result = await f(*request['params'])
            response['result'] = result
        except:
            print(sys.exc_info()[0])
            response['error'] = { 'code':-32603, 'message':'Internal error'}
    else:
        response['error'] = { 'code':-32601, 'message':'Method not found'}

    print("response: ", json.dumps(response))
    writer.write(json.dumps(response).encode('utf-8'))
    await writer.drain()
    # print("Close the client socket")
    writer.close()

class stxetx_protocol(asyncio.Protocol):
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        global current_sound
        global config
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        if len(message)>2 and message[0]=='\2' and message[-1]=='\3':
            message = message[1:-1]
            print('matched: '+ message)
            if current_sound:
                current_sound.stop()
            current_sound = None
            if message in config["sounds"]:
                print(config["sounds"][message])
                sound(*config["sounds"][message])

async def blink_led():
    while True:
        ''' blink pause blink forever '''
        led_on()
        await asyncio.sleep(0.3)
        if not sound_isactive:
            led_off()
        await asyncio.sleep(0.1)

        led_on()
        await asyncio.sleep(0.3)
        if not sound_isactive:
            led_off()

        await asyncio.sleep(6)

def main(args):
    global config, RPC_PORT, UDP_PORT

    print(args)

    pygame.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=64)
    pygame.init()
    pygame.mixer.init()

    # load config
    config = {"version":version, "rpc_port":RPC_PORT, "udp_port":UDP_PORT, "name":"" }
    with open('sound_machine.json', 'r') as infile:
        config.update(json.load(infile))
    print(config)

    loop = asyncio.get_event_loop()
    coro_tcp = asyncio.start_server(handle_jsonrpc, '0.0.0.0', RPC_PORT, loop=loop)
    server = loop.run_until_complete(coro_tcp)
    t_udp = loop.create_datagram_endpoint(
        stxetx_protocol,
        local_addr=('127.0.0.1', config["udp_port"]))
    loop.run_until_complete(t_udp)
    loop.run_until_complete(blink_led())

    # Serve requests until Ctrl+C is pressed
    print('Serving on {}'.format(server.sockets[0].getsockname()))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    # Close the server
    t_udp.close()
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()

if __name__ == '__main__':
    main(sys.argv)
