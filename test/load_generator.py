#!/usr/bin/env python3

''' test load generator 
'''


import socket
import json
import time
import datetime

rpchost = '192.168.178.66'
rpcport = 6002


def jsonRPC(method, parameter = [], method_id=0):
    ''' jsonRPC client implementation
    usage: 
    jsonRPC("printMessage", ["hello from other planet!"])
    jsonRPC("resetModem")
    '''
    payload = {
        "method": method,
        "params": parameter,
        "jsonrpc": "2.0",
        "id": method_id,
    }
    # create a socket object
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # connection to hostname on the port.
        s.connect((rpchost, rpcport))                               

        s.send(str.encode(json.dumps(payload) + '\n'))
        print(json.dumps(payload))
        # Receive no more than 1024 bytes
        msg = s.recv(1024)                                     

        s.close()
        response = json.loads(msg.decode('ascii'))
        if 'result' in response:
            try:
                # print (msg.decode('ascii'))
                result = response['result']
                # print(result)
                return result
            except:
                pass
        else:
            print(response['error'])

if __name__ == "__main__":
    for i in range(1000):
        print('loop: ', i)
        try:
            print(jsonRPC('ping', ['message'], i))
            print(jsonRPC('echo', ['message'], i))
            print(jsonRPC('add', [1, 4], i))
            print(jsonRPC('adc', [1], i))
        except:
            pass
        time.sleep(2.0)
