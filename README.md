# sound machine

simple program demonstrating async features and learning async

# task

- application specific protocol
- JSON RPC Client
- play sound on request
- periodic LED blinking

# more advanced

- change LED blinking according activity
- sound could have defined length or play whole soundfile
- sound could be interrupted by command stop






